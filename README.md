# OERcamp 2019 Hannover

Alle Informationen zu dem Workshop "Gemeinsame Erstellung und Bearbeitung von freien Lehr- und Lernmaterialien in Gitlab"

# Folien zum Workshop

* [Ansicht](https://axel-klinger.gitlab.io/oer-slides/oer-ws-1-haj.html#/title-slide)
* [Quelle](https://gitlab.com/axel-klinger/oer-slides/blob/master/markdown/oer-ws-1-haj.md)
* [Kurs Beispiel](https://gitlab.com/axel-klinger/baustatik-teil-1)

# Referenz zu Markdown in GitLab

* [Dokumentation GitLab](https://docs.gitlab.com/ee/user/markdown.html)

# Beispielkurs / Projekte

* [Course Metadata Test](https://gitlab.com/TIBHannover/oer/course-metadata-test)
* [Studienarbeit](https://gitlab.com/axel-klinger/studienarbeit)
* [OER-Würfel](http://oer-wuerfel.de) und die [Quellen](https://gitlab.com/TIBHannover/oer/oer-cube-page)
* [Folien](https://gitlab.com/axel-klinger/oer-slides)

# Informationen vom OERcamp 2019 in Lübeck

* [Workshops in Lübeck](https://gitlab.com/axel-klinger/oercamp-2019-luebeck)